from flask import render_template, flash, redirect, request, url_for
from flask_login import login_user, logout_user
from app import app, db, lm

from app.models.tables import User
from app.models.forms import LoginForm

from connect import atualizarPessoa, buscarPorCPF, count, deletarPessoa, inserir, mostrarTabelaTodos, mostrarTodos
from model import Pessoa
from flask import Flask, render_template, redirect
import pandas as pd
from flask.globals import request


from flask import Flask, session


app.secret_key = "super secret key"

@lm.user_loader
def load_user(id):
    return User.query.filter_by(id=id).first()


@app.route('/telaprincipal', methods = ['GET', 'POST'])
def index():
    if request.method == "POST":
        nome = (request.form['nome'])
        peso =  (request.form['peso'])
        altura = (request.form['altura'])
        imc = round(float(peso)/(float(altura)/100)**2, 2)
        
        return render_template('index.html', nome=nome, peso=peso, altura=altura, imc=imc)
        

    return render_template("index.html")

# @app.route('/medidas')
# def medidas():
#     return render_template ("avaliacao.html")
    

@app.route('/lista')
def lista():
    return render_template ("listadealuno.html")
    


@app.route('/', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit( ):
        user = User.query.filter_by(username=form.username.data).first()
        if user and user.password == form.password.data:
            # login_user(user)
            flash("logged in")
            return redirect(url_for("index"))
            
        else:
            flash("invalido login")    
   
    return render_template('login.html', form=form)

@app.route("/logout")
def logout():
    # logout_user()
    flash("Logged out.")
    return redirect(url_for("index"))



@app.route('/ava')
def ava():
    result = mostrarTodos()
    total_cadastrado = count()
    return render_template("ava.html",
    result = result,
    total = total_cadastrado)

@app.route("/cadastrar", methods=["POST","GET"])
def cadastrar():
    nome = request.form["nome"]
    cpf = request.form["cpf"]
    dataNascimento = request.form["dataNascimento"]
    pessoa = Pessoa(cpf,nome,dataNascimento)
    inserir(pessoa)
    return redirect("/ava")

@app.route('/atualizar/<int:cpf>',methods=["POST","GET"])

def atualizar(cpf):    
    if request.method =='POST':
        nome = request.form["nome"]
        cpf = request.form["cpf"]
        dataNascimento = request.form["dataNascimento"]
        pessoa = Pessoa(cpf,nome,dataNascimento)
        try:
            atualizarPessoa(cpf,pessoa)
            return redirect("/ava")
        except:
            return 'algo deu errado'
    else:
        pessoa = buscarPorCPF(cpf)
        return render_template('update.html',pessoa = pessoa)
        


@app.route('/deletar/<int:cpf>')
def deletar(cpf):
    try:
        deletarPessoa(cpf)
        return redirect("/ava")
    except:
        return "Algo de errado aconteceu"


@app.route('/medidas')
def medidas():
    return render_template ("avaliacao.html")

# @app.route('/medidas')
# def medidas():
#     return render_template ("avaliacao.html")
# @app.route('/avaliacao')
# def avaliacao():
#     result = mostrarTodos()
#     total_cadastrado = count()
#     return render_template("avaliacao.html",
#     result = result,
#     total = total_cadastrado)



# @app.route("/cadastrarPerimetro", methods=["POST","GET"])
# def cadastrarperimetro():
#     torax = request.form["torax"]
#     abdomen = request.form["abdomen"]
#     cintura = request.form["cintura"]
#     quadril = request.form["quadril"]
#     Perimetro = Perimetro(torax,abdomen,cintura, quadril)
#     inserir(Perimetro)
#     return redirect("/avaliacao")



# @app.route('/atualizar/',methods=["POST","GET"])
# def atualizarPerimetro(perimetro):    
#     if request.method =='POST':
#         torax = request.form["torax"]
#         abdomen = request.form["abdomen"]
#         cintura = request.form["cintura"]
#         quadril = request.form["quadril"]
#         perimetro = Perimetro(torax,abdomen,cintura, quadril)
#         try:
#             atualizarPerimetro(perimetro)
#             return redirect("/avaliacao")
#         except:
#             return 'algo deu errado'
#     else:
#         perimetro = buscarPorCPF(torax, abdomen, cintura, quadril)
#         return render_template('updateAvaliacao.html',perimetro = Perimetro)
        


# @app.route('/deletar/')
# def deletarPerimetro(perimetro):
#     try:
#         deletarPerimetro(perimetro)
#         return redirect("/avaliacao")
#     except:
#         return "Algo de errado aconteceu"


# @app.route("/teste/<info>")
# # 
# def teste(info):     
#     return "ok"
# @app.route("/teste")
# @app.route("/teste", defaults={"info": None})
# def teste(info):
#     r = User.query.filter_by(username="joaopedro").first()
#        print(r.username, r.name)
# #     return "ok"



@app.route('/treino')
def treino():
    return render_template('tabela.html')


# from flask import Flask, render_template, request, redirect
# from flask_mail import Mail, Message
# from reportlab.pdfgen import canvas

# app = Flask(__name__)
# app.config['MAIL_SERVER'] = 'smtp.gmail.com'
# app.config['MAIL_PORT'] = 465
# app.config['MAIL_USE_SSL'] = True
# app.config['MAIL_USERNAME'] = 'jpedromeloo123@gmail.com'  # Insira seu e-mail
# app.config['MAIL_PASSWORD'] = 'sua-senha'  # Insira sua senha
# app.config['MAIL_DEFAULT_SENDER'] = 'jpedromeloo123@gmail.com'  # Insira seu e-mail

# mail = Mail(app)

# # Lista de treinos
# treinos_a = []
# treinos_b = []
# treinos_c = []

# # Rota inicial
# @app.route('/')
# def index():
#     return render_template('tabela.html', treinos_a=treinos_a, treinos_b=treinos_b, treinos_c=treinos_c)

# # Rota para adicionar treino
# @app.route('/adicionar', methods=['POST'])
# def adicionar_treino():
#     serie = request.form.get('serie')
#     tipo_treino = request.form.get('tipo_treino')
#     qtd_series = request.form.get('qtd_series')

#     treino = {'tipo_treino': tipo_treino, 'qtd_series': qtd_series}

#     if serie == 'a':
#         treinos_a.append(treino)
#     elif serie == 'b':
#         treinos_b.append(treino)
#     elif serie == 'c':
#         treinos_c.append(treino)

#     return redirect('/')

# # Rota para gerar e enviar o treino em PDF por e-mail
# @app.route('/enviar_email', methods=['POST'])
# def enviar_email():
#     email = request.form.get('email')

#     # Gerar o arquivo PDF do treino
#     pdf = canvas.Canvas('treino.pdf')
#     pdf.setFont("Helvetica", 12)
#     pdf.drawString(100, 700, "Treino Série A")
#     pdf.drawString(100, 680, "-----------------------------------")
#     y = 660
#     for treino in treinos_a:
#         pdf.drawString(100, y, f"{treino['tipo_treino']}: {treino['qtd_series']} séries")
#         y -= 20
#     pdf.showPage()

#     pdf.setFont("Helvetica", 12)
#     pdf.drawString(100, 700, "Treino Série B")
#     pdf.drawString(100, 680, "-----------------------------------")
#     y = 660
#     for treino in treinos_b:
#         pdf.drawString(100, y, f"{treino['tipo_treino']}: {treino['qtd_series']} séries")
#         y -= 20
#     pdf.showPage()

#     pdf.setFont("Helvetica", 12)
#     pdf.drawString(100, 700, "Treino Série C")
#     pdf.drawString(100, 680, "-----------------------------------")
#     y = 660
#     for treino in treinos_c:
#         pdf.drawString(100, y, f"{treino['tipo_treino']}: {treino['qtd_series']} séries")
#         y -= 20
#     pdf.save()

#     # Enviar o e-mail com o anexo do treino em PDF
#     msg = Message("Treino de Educação Física", recipients=[email])
#     msg.body = "Segue em anexo o treino de Educação Física."
#     with app.open_resource("treino.pdf") as fp:
#         msg.attach("treino.pdf", "application/pdf", fp.read())
#     mail.send(msg)

#     return redirect('/')

# if __name__ == '__main__':
#     app.run()
